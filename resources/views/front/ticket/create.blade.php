@extends('front.master')


@section('content')
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Poster un ticket</h2>

                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <br/>
                    <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" method="post" action="{{ url('/ticket/store') }}">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Objet <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" name="object" value="{{ old('object') }}" required="required" class="form-control col-md-7 col-xs-12">
                                @if ($errors->has('object'))
                                    <span class="help-block text-danger">
                                        <strong>{{ $errors->first('object') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="priority">Priorité</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <select class="form-control" id="priority" name="priority" required>
                                    <option disabled selected>Choisir..</option>
                                    <option value="Normale">Normale</option>
                                    <option value="Haute">Haute</option>
                                    <option value="Urgente">Urgente</option>
                                </select>
                                @if ($errors->has('priority'))
                                    <span class="help-block text-danger">
                                        <strong>{{ $errors->first('priority') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Text</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <textarea required="required" rows="10" class="form-control" name="content">{{ old('content') }}</textarea>
                                @if ($errors->has('content'))
                                    <span class="help-block text-danger">
                                        <strong>{{ $errors->first('content') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        @include('errors/error')
                        <div class="ln_solid"></div>
                        <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                <button class="btn btn-danger" type="button">Annuler</button>
                                <button class="btn btn-warning" type="reset">Reset</button>
                                <button type="submit" class="btn btn-success pull-right">Soumettre</button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection