<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap -->
    <link href="{{ asset('libs/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="{{ asset('libs/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
    <!-- NProgress -->
    <link href="{{ asset('libs/nprogress/nprogress.css" rel="stylesheet') }}">
    <!-- iCheck -->
    <link href="{{ asset('libs/iCheck/skins/flat/green.css" rel="stylesheet') }}">
    <!-- bootstrap-progressbar -->
    <link href="{{ asset('libs/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css') }}" rel="stylesheet">
    <!-- JQVMap -->
    <link href="{{ asset('libs/jqvmap/dist/jqvmap.min.css') }}" rel="stylesheet"/>
    <!-- bootstrap-daterangepicker -->
    <link href="{{ asset('libs/bootstrap-daterangepicker/daterangepicker.css') }}" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="{{ asset('css/custom.css') }}" rel="stylesheet">
    <link href="{{ asset('css/profile.css') }}" rel="stylesheet">

    @yield('css')

    <title>Laravel</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">


</head>
<body class="nav-md">
<div class="container body">
    <div class="main_container">
        <div class="col-md-3 left_col">
            <div class="left_col scroll-view">
                <div class="navbar nav_title" style="border: 0;">
                    <a href="{{ url('/') }}" class="site_title"><i class="fa fa-building-o"></i><span> Groupe Akad</span></a>
                </div>

                <div class="clearfix"></div>

                <!-- menu profile quick info -->
                <div class="profile clearfix">
                    <div class="profile_pic">
                        <img src="{{ asset('storage/'.Auth::user()['avatar']) }}" alt="logo" class="img-circle profile_img">
                    </div>
                    <div class="profile_info">
                        <span>Bonjour,</span>
                        <h2>{{ session()->get('user')['givenname'][0] }}</h2>
                    </div>
                </div>
                <!-- /menu profile quick info -->

                <br />

                <!-- sidebar menu -->
                <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                    <div class="menu_section">
                        <h3>Generale</h3>
                        <ul class="nav side-menu">
                            <li>
                                <a href="{{ url('/') }}"><i class="fa fa-home"></i> Accueil</a>
                            </li>
                            <li><a><i class="fa fa-edit"></i> Support <span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    <li><a href="{{ url('ticket') }}">Mes tickets</a></li>
                                    <li><a href="{{ url('ticket/create') }}">Creer un ticket</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>

                </div>
                <!-- /sidebar menu -->

                <!-- /menu footer buttons -->

                <!-- /menu footer buttons -->
            </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
            <div class="nav_menu">
                <nav>
                    <div class="nav toggle">
                        <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                    </div>

                    <ul class="nav navbar-nav navbar-right">
                        <li class="">
                            <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                <img src="{{ asset('storage/'.Auth::user()['avatar']) }}" alt=""> {{ strtoupper(session()->get('user')['sn'][0]) }} {{ session()->get('user')['givenname'][0] }}
                                <span class=" fa fa-angle-down"></span>
                            </a>
                            <ul class="dropdown-menu dropdown-usermenu pull-right">
                                <li><a href="{{ url('/profile') }}"> Profile</a></li>
                                <li><a href="{{ route('logout') }}"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
                            </ul>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
            @yield('content')
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
            <div class="pull-right">
                Groupe Akad 2017
            </div>
            <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
    </div>
</div>






<!-- jQuery -->
<script src="{{ asset('libs/jquery/dist/jquery.min.js') }}"></script>
<!-- Bootstrap -->
<script src="{{ asset('libs/bootstrap/dist/js/bootstrap.min.js') }}"></script>
<!-- FastClick -->
<script src="{{ asset('libs/fastclick/lib/fastclick.js') }}"></script>
<!-- NProgress -->
<script src="{{ asset('libs/nprogress/nprogress.js') }}"></script>
<!-- Chart.js -->
<script src="{{ asset('libs/Chart.js/dist/Chart.min.js') }}"></script>
<!-- gauge.js -->
<script src="{{ asset('libs/gauge.js/dist/gauge.min.js') }}"></script>
<!-- bootstrap-progressbar -->
<script src="{{ asset('libs/bootstrap-progressbar/bootstrap-progressbar.min.js') }}"></script>
<!-- iCheck -->
<script src="{{ asset('libs/iCheck/icheck.min.js') }}"></script>
<!-- Skycons -->
<script src="{{ asset('libs/skycons/skycons.js') }}"></script>
<!-- Flot -->
<script src="{{ asset('libs/Flot/jquery.flot.js') }}"></script>
<script src="{{ asset('libs/Flot/jquery.flot.pie.js') }}"></script>
<script src="{{ asset('libs/Flot/jquery.flot.time.js') }}"></script>
<script src="{{ asset('libs/Flot/jquery.flot.stack.js') }}"></script>
<script src="{{ asset('libs/Flot/jquery.flot.resize.js') }}"></script>
<!-- Flot plugins -->
<script src="{{ asset('libs/flot.orderbars/js/jquery.flot.orderBars.js') }}"></script>
<script src="{{ asset('libs/flot-spline/js/jquery.flot.spline.min.js') }}"></script>
<script src="{{ asset('libs/flot.curvedlines/curvedLines.js') }}"></script>
<!-- DateJS -->
<script src="{{ asset('libs/DateJS/build/date.js') }}"></script>
<!-- JQVMap -->
<script src="{{ asset('libs/jqvmap/dist/jquery.vmap.js') }}"></script>
<script src="{{ asset('libs/jqvmap/dist/maps/jquery.vmap.world.js') }}"></script>
<script src="{{ asset('libs/jqvmap/examples/js/jquery.vmap.sampledata.js') }}"></script>
<!-- bootstrap-daterangepicker -->
<script src="{{ asset('libs/moment/min/moment.min.js') }}"></script>
<script src="{{ asset('libs/bootstrap-daterangepicker/daterangepicker.js') }}"></script>

<!-- Custom Theme Scripts -->
<script src="{{ asset('js/custom.js') }}"></script>
<script src="{{ asset('js/profile.js') }}"></script>
@yield('script')
</body>
</html>
