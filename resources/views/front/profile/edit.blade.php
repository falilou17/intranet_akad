@extends('front.master')
@section('content')
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Profile</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <br/>
                    <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" enctype="multipart/form-data" method="post"
                          action="{{ url('/profile') }}">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <div class="col-md-12 col-sm-12 col-xs-12 avatar_profile">
                                <label for="avatar">
                                    <img id="avatar_profile" src="{{ asset('storage/'.Auth::user()['avatar']) }}" alt="logo"
                                         class="img-circle img-responsive">
                                </label>
                                <input id="avatar" name="avatar" type="file" accept="image/*"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Nom <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" name="firstname" value="{{ session()->get('user')['givenname'][0] }}"
                                       required="required" class="form-control col-md-7 col-xs-12">
                                @if ($errors->has('firstname'))
                                    <span class="help-block text-danger">
                                        <strong>{{ $errors->first('firstname') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Prénom <span
                                        class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" name="lastname" value="{{ session()->get('user')['sn'][0] }}"
                                       required="required" class="form-control col-md-7 col-xs-12">
                                @if ($errors->has('lastname'))
                                    <span class="help-block text-danger">
                                        <strong>{{ $errors->first('lastname') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Email <span
                                        class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" disabled required="required"
                                       value="{{ session()->get('user')['mail'][0] }}"
                                       class="form-control col-md-7 col-xs-12">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Société</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <select class="form-control" name="society" required>
                                    @foreach($societies as $society)
                                        <option {{ $society->id == $user->society->id ? 'selected' : '' }} value="{{ $society->id }}">{{ $society->name }}</option>
                                    @endforeach
                                </select>
                                @if ($errors->has('society'))
                                    <span class="help-block text-danger">
                                        <strong>{{ $errors->first('society') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Fonction</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" name="job" value="{{ $user->job }}" required="required"
                                       class="form-control col-md-7 col-xs-12">
                                @if ($errors->has('job'))
                                    <span class="help-block text-danger">
                                        <strong>{{ $errors->first('job') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        @include('errors/error')
                        <div class="ln_solid"></div>
                        <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                <button class="btn btn-danger" type="button">Annuler</button>
                                <a class="btn btn-warning" role="button" href="{{url('/password')}}">Changer de Mot de
                                    Passe</a>
                                <button type="submit" class="btn btn-success pull-right">Soumettre</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection