@if(session()->get('type'))
    <span style="font-size: 12px">
        <div class="row text-center ">
            <div class="col-sm-offset-4 col-sm-4">
                <div class=" center-block alert alert-{{ session()->get('type') }} alert-dismissable" style="padding: 0;"
                     role="alert">
                    <p>{{ session()->get('msg') }}</p>
                </div>
            </div>
        </div>
    </span>
@endif