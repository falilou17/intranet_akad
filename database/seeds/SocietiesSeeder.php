<?php

use Illuminate\Database\Seeder;

class SocietiesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('societies')->insert([
            'id'     =>      '1',
            'name'  =>      'JNP'
        ]);

        DB::table('societies')->insert([
            'id'     =>      '2',
            'name'  =>      'SOCOTEB'
        ]);

        DB::table('societies')->insert([
            'id'     =>      '3',
            'name'  =>      'SBTF SARL'
        ]);

        DB::table('societies')->insert([
            'id'     =>      '4',
            'name'  =>      'SBTF TOURS'
        ]);

        DB::table('societies')->insert([
            'id'     =>      '5',
            'name'  =>      'OCEAN EMIRATES'
        ]);

        DB::table('societies')->insert([
            'id'     =>      '6',
            'name'  =>      'OCEAN GENEVE'
        ]);

        DB::table('societies')->insert([
            'id'     =>      '7',
            'name'  =>      'HOLDING'
        ]);
    }
}
