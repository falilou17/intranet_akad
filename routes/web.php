<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('login', ['as' => 'login', 'uses' => 'Auth\LoginController@showLoginForm']);
Route::post('login', ['as' => 'login', 'uses' => 'AuthController@login']);

Route::group(['middleware' => ['auth']], function () {
    Route::get('/', ['as' => 'home', 'uses' => 'HomeController@index']);
    Route::group(['prefix' => 'ticket'], function () {
        Route::get('', ['as' => 'tickets', 'uses' => 'TicketController@index']);
        Route::get('create', ['as' => 'ticket.create', 'uses' => 'TicketController@create']);
        Route::post('store', ['as' => 'ticket.store', 'uses' => 'TicketController@store']);
        Route::get('show/{id}', ['as' => 'ticket.show', 'uses' => 'TicketController@show']);
        Route::get('destroy/{id}', ['as' => 'ticket.destroy', 'uses' => 'TicketController@destroy']);
    });
    Route::get('/profile', ['as' => 'profile', 'uses' => 'ProfileController@edit']);
    Route::post('/profile', ['as' => 'profile.update', 'uses' => 'ProfileController@update']);
    Route::get('/password', ['as' => 'password', 'uses' => 'ProfileController@password']);
    Route::post('/password', ['as' => 'password.update', 'uses' => 'ProfileController@update_password']);

    Route::get('/logout', ['as' => 'logout', 'uses' => 'AuthController@logout']);
    Route::get('/auth/logout', ['as' => 'logout.auth', 'uses' => 'Auth\LoginController@logout']);
});