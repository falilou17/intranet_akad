<?php

namespace App\Http\Controllers;

use App\Http\Requests\TicketRequest;
use App\Models\Job;
use App\Models\Society;
use App\Models\Ticket;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TicketController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tickets = Ticket::where('user_id', session()->get('user')['uidnumber'][0])->get();
        return view('front.ticket.index', compact('tickets'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('front/ticket/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param TicketRequest|Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(TicketRequest $request)
    {
        try {
            Ticket::create([
                'object' => $request->input('object'),
                'content' => $request->input('content'),
                'priority' => $request->input('priority'),
                'user_id' => Auth::user()->getAuthIdentifier()
            ]);

            return redirect()
                ->back()
                ->with('type', 'success')
                ->with('msg', 'Le ticket a été soumis');
        } catch (Exception $e) {
            return redirect()
                ->back()
                ->with('type', 'error')
                ->with('msg', 'Une erreur est survenue');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $ticket = Ticket::where('id', $id)->where('user_id', session()->get('user')['uidnumber'][0])->first();
        if ($ticket)
            return view('front.ticket.show', compact('ticket'));
        else
            return redirect()
                ->route('tickets')
                ->with('type', 'error')
                ->with('msg', 'le ticket que vous cherchez n\'existe pas');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $ticket = Ticket::where('id', $id)->where('user_id', session()->get('user')['uidnumber'][0])->first();
            if ($ticket) {
                $ticket->delete();
                return redirect('/ticket')
                    ->with('type', 'success')
                    ->with('msg', 'Le ticket a été supprimer');
            } else {
                return redirect()
                    ->back()
                    ->with('type', 'success')
                    ->with('msg', 'Le ticket n\'existe pas');
            }
        } catch (Exception $e) {
            return redirect()
                ->back()
                ->with('type', 'error')
                ->with('msg', 'Une erreur est survenue');
        }
    }
}
