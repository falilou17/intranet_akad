<?php

namespace App\Http\Controllers;

use App\Models\User;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Zend\Ldap\Ldap;

class AuthController extends Controller
{
    protected $ldap;
    protected $baseDn = 'dc=groupeakad,dc=com';

    public function __construct()
    {
        $options = array(
            'host' => '10.40.8.10',
            'password' => 'Ak@d2016*#',
            'bindRequiresDn' => true,
            'baseDn' => 'dc=groupeakad,dc=com',
            'username' => "cn=admin,$this->baseDn"
        );
        $this->ldap = new Ldap($options);
        $this->ldap->bind();
    }

    public function login(Request $request)
    {
        $user = $this->ldap->search(
            '(mail=' . $request->email . ')',
            "$this->baseDn",
            Ldap::SEARCH_SCOPE_SUB
        )->getFirst();
        if (count($user)) {
            try {
                $this->ldap->bind($user['uid'][0], $request->password);
                $userLog = User::find($user['uidnumber'][0]);
                if (!$userLog) {
                    $userLog = User::create([
                        'id' => $user['uidnumber'][0],
                        'society_id' => 1,
                        'job' => ''
                    ]);
                }
                session(['user' => $user]);
                Auth::login($userLog);
                return redirect()
                    ->route('home')
                    ->with('type', 'success')
                    ->with('msg', 'Bienvenue ' . $user['givenname'][0]);
            } catch (Exception $e) {
                return redirect()
                    ->back()
                    ->with('type', 'error')
                    ->with('msg', 'Email/Mot de passe incorrect');
            }
        } else
            return redirect()
                ->back()
                ->with('type', 'error')
                ->with('msg', 'Email/Mot de passe incorrect');
    }

    public function logout()
    {
        session()->forget('user');
        return redirect()
            ->route('logout.auth');
    }
}
