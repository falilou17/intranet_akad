<?php

namespace App\Http\Controllers;

use App\Http\Requests\PasswordRequest;
use App\Http\Requests\ProfileRequest;
use App\Models\Society;
use App\Models\User;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Zend\Ldap\Ldap;

class ProfileController extends Controller
{
    protected $ldap;
    protected $baseDn = 'dc=groupeakad,dc=com';

    public function __construct()
    {
        $options = array(
            'host' => '10.40.8.10',
            'password' => 'Ak@d2016*#',
            'bindRequiresDn' => true,
            'baseDn' => 'dc=groupeakad,dc=com',
            'username' => "cn=admin,$this->baseDn"
        );
        $this->ldap = new Ldap($options);
        $this->ldap->bind();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function password()
    {
        $user = session()->get('user');
        return view('front.profile.password', compact('user'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param PasswordRequest|Request $request
     * @return \Illuminate\Http\Response
     */
    public function update_password(PasswordRequest $request)
    {
        try{
            $userldap = $this->ldap->search(
                '(mail=' . session()->get('user')['mail'][0] . ')',
                "$this->baseDn",
                Ldap::SEARCH_SCOPE_SUB
            )->getFirst();
            $this->ldap->bind($userldap['uid'][0], $request->input('old_password'));
            $this->ldap->update(session()->get('user')['dn'],[
                'userpassword' => $request->input('new_password')
            ]);
            return redirect('/profile')
                ->with('type', 'success')
                ->with('msg', 'Le mot de passe a été mis à jour');
        }catch (Exception $e){
            return redirect()
                ->back()
                ->with('type', 'error')
                ->with('msg', 'Le mot de passe est erroné');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        $user = User::find(Auth::user()->getAuthIdentifier());
        $societies = Society::all();
        return view('front.profile.edit', compact('user', 'societies'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param ProfileRequest|Request $request
     * @return \Illuminate\Http\Response
     */
    public function update(ProfileRequest $request)
    {
        try{
            $user = User::find(session()->get('user')['uidnumber'][0]);
            $user->update([
                'society_id' => $request->input('society'),
                'job' => $request->input('job'),
            ]);
            $this->ldap->update(session()->get('user')['dn'],[
                'givenname' => $request->firstname,
                'sn' => $request->lastname
            ]);
            if ($request->hasFile('avatar')) {
                $fileName = Storage::putFile('public', $request->file('avatar'));
                $user->avatar = substr($fileName, 7, strlen($fileName));
                $user->save();
            }
            $userldap = $this->ldap->search(
                '(mail=' . session()->get('user')['mail'][0] . ')',
                "$this->baseDn",
                Ldap::SEARCH_SCOPE_SUB
            )->getFirst();
            session(['user' => $userldap]);
            return redirect()
                ->back()
                ->with('type', 'success')
                ->with('msg', 'Le profile a été mis à jour');
        }catch (Exception $e){
            return redirect()
                ->back()
                ->with('type', 'error')
                ->with('msg', 'Une erreur est survenue');
        }
    }
}
